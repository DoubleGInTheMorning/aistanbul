//-----------------------------------------------------------------------
// <copyright file="ForwardGeocodeUserInput.cs" company="Mapbox">
//     Copyright (c) 2016 Mapbox. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Mapbox.Examples
{
	using Mapbox.Unity;
	using UnityEngine;
	using UnityEngine.UI;
	using System;
	using Mapbox.Geocoding;
	using Mapbox.Utils;
    using Mapbox.Unity.MeshGeneration.Components;
    using Mapbox.Unity.MeshGeneration.Modifiers;

    [RequireComponent(typeof(InputField))]
	public class CustomForwardGeocodeUserInput : ForwardGeocodeUserInput
	{

        public delegate void OnSearchFilter(string filter);
        OnSearchFilter searchCallback;

		void Awake()
		{
            _inputField = GetComponent<InputField>();
            _inputField.onValueChanged.AddListener(HandleUserInput);
        }

        private void Start()
        {
        }


        void HandleUserInput(string searchString)
		{
            //foreach (CustomFeatureBehaviour feature in CustomFeatureBehaviourModifier.featureList)
            //{
            //    if (feature.VectorEntity.Feature.Properties.ContainsKey("name"))
            //    {
            //        if (!feature.VectorEntity.Feature.Properties["name"].ToString().ToLower().Contains(searchString.ToLower()))                    
            //            feature.gameObject.SetActive(false);                   
            //        else                    
            //            feature.gameObject.SetActive(true);                    
            //    }
            //}
            CustomFeatureBehaviourModifier.OnSearchFilter(searchString);
            //searchCallback(searchString);
        }

		void HandleGeocoderResponse(ForwardGeocodeResponse res)
		{
            base.HandleGeocoderResponse(res);
		}
	}
}
