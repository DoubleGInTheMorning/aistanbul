using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Mapbox.Utils;
using Mapbox.Geocoding;

public class QuestionManager : MonoBehaviour {

    [Header("Database")]
    public List<string> _uploadURLs;

    [Header("UI Manager")]
    [SerializeField]
    private UIManager _uiManager;

    [Header("UI Objects")]
    [SerializeField]
    private Image _panel;
    [SerializeField]
    private Text _questionText;
    [SerializeField]
    private GameObject _questionBackground;
    [SerializeField]
    private DistrictSelectionHandler _districtHandler;
    [SerializeField]
    private Dropdown _answerDropdown;
    [SerializeField]
    private GameObject _dropdownParent;
    [SerializeField]
    private InputField _inputField;
    [SerializeField]
    private Text _inputQuestionText;

    [SerializeField]
    private Button _submitInputField;
    [SerializeField]
    private MapSelector _mapSelector;
    [SerializeField]
    private GameObject _categoryGameObject;
    [SerializeField]
    private GameObject _categoryQuestion;
    [SerializeField]
    private Text _categoryQuestionText;

    [Header("Buttons")]
    [SerializeField]
    private Button _skipButton;

    [Header("Category Button Prefab")]
    [SerializeField]
    private Button _categoryButton;
    private string _currentCategory;

    private Dictionary<string, GameObject> _displayDict;
    private GameObject _currDisplay;    

    //text files
    [Header("Question Data")]
    [SerializeField]
    private TextAsset _questionsJson;    
    private RootObject _questionData;

    //question containers
    private Dictionary<string, List<Question>> _questionListByCategory; //quesiton list by category
    private List<Question> _currentQuestionList;
    private Dictionary<string, Button> _categoryButtonDict;
    private int _remainingCategoryCount;
    private int _categoryCount;
    private int _currentQuestionIndex;
    private Question _currentQuestion;

    struct FollowUpQuestions
    {
        public List<FollowUp> questions;
        public int currIndex;
        public string response;

        public FollowUpQuestions(List<FollowUp> followUps, string answer, int index = 0)
        {
            questions = followUps;
            currIndex = index;
            response = answer;
        }
    }

    private Stack<FollowUpQuestions> _followUpQuestionsStack;    
    private string _firstCategory;

    [System.Serializable]
    public class Answer
    {
        public string Qid;//question id
        public string Category;
        public string Timestamp;
        public string Type;
        public string FeatureType;
        public string FeatureName;
        public string Lat;
        public string Lng;
        public string Response;       
    }
    [System.Serializable]
    public class Session
    {
        public string id;
        public List<Answer> answers;
    }

    private Session _sessions;
    int _sessionIndex;

    private void Awake()
    {
        _displayDict = new Dictionary<string, GameObject>();

        _displayDict.Add("question", _questionBackground);
        _displayDict.Add("category", _categoryGameObject);
        _displayDict.Add("dropdown", _dropdownParent);
        _displayDict.Add("map", _mapSelector.gameObject);
        _displayDict.Add("input", _inputField.gameObject);

        _dropdownParent.GetComponentInChildren<Button>().onClick.AddListener(OnSubmitDropdown);
        _mapSelector.selectCallback = OnSelectMapPoint;
        _mapSelector.onDisableQuestionUICallback = OnSelectMapButton;
        _mapSelector.onEnableQuestionUICallback = OnSelectMapPoint;

        _submitInputField.onClick.AddListener(OnAnswerInputField);
        _skipButton.onClick.AddListener(OnSkipButton);
    }

    // Use this for initialization
    void Start ()
    {
        _questionData = JsonUtility.FromJson<RootObject>(_questionsJson.text);
        _firstCategory = _questionData.firstCategory;

        _questionListByCategory = new Dictionary<string, List<Question>>();
        foreach (string category in _questionData.category)
            _questionListByCategory.Add(category, new List<Question>());

        foreach (Question question in _questionData.questions)
            _questionListByCategory[question.category].Add(question);

        
        InitCategoryQuestions();
        StartNewSession();
    }
	
    //creates a new session for each user  
    public void StartNewSession()
    {
        if (_sessions == null)
            _sessions = new Session();

        //creates a new session
        //unique id is the time down to seconds that the user is created + a count    
        string id = "";
        for (int i = 0; i < 9; i++)
            id += UnityEngine.Random.Range(0, 10).ToString();

        Session session = new Session();
        session.id = id;
        session.answers = new List<Answer>();

        _sessions = session;

        //set up the first question List
        ResetCategory();
        OnSelectCategory(_firstCategory);
    }

    void InitCategoryQuestions()
    {
        GameObject categoryObj = _categoryQuestion;
        _currDisplay = _categoryGameObject;

        _questionText.text = "Which categories do you want to answer";

        _categoryCount = _questionData.category.Count;
        _remainingCategoryCount = _categoryCount;

        if (_categoryButtonDict == null)
            _categoryButtonDict = new Dictionary<string, Button>();

        foreach (string category in _questionData.category)
        {
            var button = Instantiate(_categoryButton);
            button.transform.SetParent(categoryObj.transform, false);
            button.GetComponentInChildren<Text>().text = category;

            _categoryButtonDict.Add(category, button);

            var clickHandler = button.GetComponent<CategoryButton>();
            clickHandler.text = category;
            clickHandler.callback = OnSelectCategory;
        }        
    }

    void ResetCategory()
    {
        _remainingCategoryCount = _categoryCount;
        foreach(var categoryButton in _categoryButtonDict)
        {
            categoryButton.Value.gameObject.SetActive(true);
        }
    }

    private Question GetCurrentQuestion()
    {
        return _currentQuestion;
    }

    private List<string> GetRequiredFollowUpAnswer()
    {
        if (_followUpQuestionsStack.Count > 0)
        {
            FollowUpQuestions followUp = _followUpQuestionsStack.Peek();
            int index = followUp.currIndex;

            return followUp.questions[index].requiredAnswers;
        }
        else
            return null;

    }

    private void SetQuestionText(string text)
    {
        _panel.enabled = true;
        _displayDict["question"].SetActive(true);
        _questionText.text = text;
    }

    private void HideQuestionText()
    {
        _panel.enabled = true;
        _displayDict["question"].SetActive(false);
    }

    private void SetCategoryQuestion()
    {       
        SetNewDisplay("category");
        HideQuestionText();
    }

    private void SetNewDisplay(string displayName)
    {
        _currDisplay.SetActive(false);

        //set new display
        _currDisplay = _displayDict[displayName];
        _currDisplay.SetActive(true);        
    }

    private void SetMapQuestion(Question question)
    {
        SetNewDisplay("map");
        
        SetQuestionText(question.text);
        _mapSelector.SetActive(question.type == "mapPoint");
    }

    private void SetDropdownQuestion(Question question)
    {
        SetNewDisplay("dropdown");

        _answerDropdown.gameObject.SetActive(true);
        _answerDropdown.ClearOptions();
        _answerDropdown.AddOptions(new List<string> { "Bir Yanıt Seçiniz / Select Answer" });
        _answerDropdown.AddOptions(question.answers);
        _answerDropdown.value = 0;
        _answerDropdown.Select();

        SetQuestionText(question.text);
    }

    private void SetInputQuestion(Question question)
    {
        SetNewDisplay("input");

        _inputField.text = "";
        _inputField.gameObject.SetActive(true);
        _inputQuestionText.text = question.text;

        //use the input question text instead
        HideQuestionText();
    }

    private void SetQuestion(int index)
    {
        if (_currentQuestionList == null)
            Debug.LogError("Question List is null");

        var question = _currentQuestionList[index];
        _currentQuestion = question;

        if (question.type == "dropdown")
            SetDropdownQuestion(question);
        else if (question.type == "map" || question.type == "mapPoint")
            SetMapQuestion(question);
        else if (question.type == "input")
            SetInputQuestion(question);
    }

    private void SetQuestion(Question question)
    {
        if (question == null)
        {
            SetNextQuestion();
            Debug.LogError("Follow Up Question is null. Cannot set it.");
            return;
        }

        _currentQuestion = question;

        if (question.type == "dropdown")
            SetDropdownQuestion(question);
        else if (question.type == "map" || question.type == "mapPoint")
            SetMapQuestion(question);
        else if (question.type == "input")
            SetInputQuestion(question);
    }

    private void SetNextQuestion()
    {
        _mapSelector.ResetState();

        _currentQuestionIndex++;        
        if (_currentQuestionIndex < _currentQuestionList.Count)
            SetQuestion(_currentQuestionIndex);
        else
        {
            if (_remainingCategoryCount > 0)
                SetCategoryQuestion();
            else
                FinishSession();
        }
    }

    private void SetNextQuestion(string response)
    {
        var question = GetCurrentQuestion();
        var followUp = question.followUp;

        //if follow up, check to move to it
        if(followUp != null && followUp.id != null)
        {
            if(followUp.requiredAnswers.Count > 0)
            {
                foreach(string ans in followUp.requiredAnswers)
                {
                    if(response == ans)
                    {
                        SetQuestion(followUp);
                        return;
                    }
                }
            }
        }
        //go to next question in root
        SetNextQuestion();
    }

    private void SetQuestionList(string category)
    {
        if (!_questionListByCategory.ContainsKey(category))
            Debug.LogError("Question Key '" + category + "' does not exist");
        else
            _currentQuestionList = _questionListByCategory[category];

        _currentQuestionIndex = 0;
        SetQuestion(_currentQuestionIndex);
    }

    private void OnSelectMapButton()
    {
        _panel.enabled = false;
        _displayDict["question"].SetActive(false);
    }

    private void OnSelectMapPoint()
    {
        _panel.enabled = true;
        _displayDict["question"].SetActive(true);
    }

    private void OnSelectMapPoint(string featureName, string featureLat, string featureLong, string featureType = "")
    {
        _displayDict["question"].SetActive(true);

        string questionID = GetCurrentQuestion().id;

        Answer answer = new Answer();
        answer.Qid = questionID;
        answer.Category = _currentCategory;
        answer.FeatureName = featureName;
        answer.Lat = featureLat;
        answer.Lng = featureLong;
        answer.FeatureType = featureType;
        answer.Type = GetCurrentQuestion().type.ToString();
        answer.Timestamp = System.DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");

        _sessions.answers.Add(answer);

        SetNextQuestion();
    }

    private void OnAnswerInputField()
    {
        Debug.Log("answer is " + _inputField.text);
        if (_inputField.text == string.Empty)
            return;

        string questionID = GetCurrentQuestion().id;

        Answer answer = new Answer();
        answer.Qid = questionID;
        answer.Category = _currentCategory;
        answer.Response = _inputField.text;
        answer.Type = GetCurrentQuestion().type.ToString();
        answer.Timestamp = System.DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");

        _sessions.answers.Add(answer);

        SetNextQuestion();
    }

    private void OnSubmitDropdown()
    {
        //cant choose "Select Answer"
        if (_answerDropdown.value == 0)
            return;

        Debug.Log("answer is " + _answerDropdown.options[_answerDropdown.value].text);
        string value = _answerDropdown.options[_answerDropdown.value].text;
        string questionID = GetCurrentQuestion().id;

        Answer answer = new Answer();
        answer.Qid = questionID;
        answer.Category = _currentCategory;
        answer.Response = value;
        answer.Type = GetCurrentQuestion().type.ToString();
        answer.Timestamp = System.DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");

        _sessions.answers.Add(answer);
        SetNextQuestion(value);
    }

    public void OnSelectCategory(string text)
    {       
        GameObject categoryObj = _displayDict["category"].gameObject;
        categoryObj.SetActive(false);
        _currentCategory = text;
        _categoryButtonDict[text].gameObject.SetActive(false);
        _remainingCategoryCount--;

        SetQuestionList(text);
    }

    public void OnSkipButton()
    {
        SetNextQuestion("");
    }

    private void FinishSession()
    {
        _uiManager.OnClickQuit();
    }

    public void OnSessionEnd()
    {
        _mapSelector.ResetState();
        SubmitAnswerData();
        _sessions = null;
        StartNewSession();
        OnSelectCategory(_firstCategory);
    }

    public void SubmitAnswerData()
    {
        //call a coroutine to upload data to DB
        //all the answers are in the sessions variable in string format with the respective question IDs

        //start new session after
        foreach (string url in _uploadURLs)
        {
            StartCoroutine(InsertAnswerData(url));
        }
        //SetNewDisplay("category");
    }

    IEnumerator InsertAnswerData(string url)
    {
        string data = JsonUtility.ToJson(_sessions);
        string postURL = string.Format(
            url
        );
        WWWForm wwwform = new WWWForm();
        wwwform.AddField("data", data);
        UnityWebRequest www = UnityWebRequest.Post(postURL, wwwform);
        www.method = "POST";

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log("ApplyDatasetToTileset www error: " + www.error);
        }
        else
        {
            Debug.Log("ApplyDatasetToTileset www = " + www.downloadHandler.text);
        }        
    }


	// Update is called once per frame
	void Update () {
		
	}
}
