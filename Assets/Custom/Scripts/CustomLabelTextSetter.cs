namespace Mapbox.Examples
{
	using Mapbox.Unity.MeshGeneration.Interfaces;
	using System.Collections.Generic;
	using UnityEngine;

	public class CustomLabelTextSetter : MonoBehaviour, IFeaturePropertySettable
	{
		[SerializeField]
		public TextMesh _textMesh; 

		public void Set(Dictionary<string, object> props)
		{
			_textMesh.text = "";

			if (props.ContainsKey("name"))
			{
				_textMesh.text = props["name"].ToString();
			}
			else if (props.ContainsKey("house_num"))
			{
				_textMesh.text = props["house_num"].ToString();
			}
			else if (props.ContainsKey("type"))
			{
				_textMesh.text = props["type"].ToString();
			}
		}

        public void SetName(string name)
        {
            _textMesh.text = name;
        }

        public void OnMouseEnter()
        {
            _textMesh.color = Color.red;
        }

        public void OnMouseExit()
        {
            _textMesh.color = Color.white;
        }

        void Update()
        {
        }
    }
}