﻿using Mapbox.Unity;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using Mapbox.Geocoding;
using Mapbox.Utils;
using Mapbox.Unity.Map;
using Mapbox.Examples;

public class AddFeature : MonoBehaviour {

    [Header("Cursor Settings")]
    public Texture2D _cursorTex;
    public CursorMode _cursorMode = CursorMode.Auto;
    public Vector2 _hotSpot;

    [Header("New Feature Prefabs")]
    public GameObject _newFeaturePrefab;
    public GameObject _customFeaturePrefab;

    [Header("Camera")]
    public CameraMovement _cameraMovement;

    [Header("Add Feature")]
    public Button _addButton;
    public GameObject _addButtonGO;

    [Header("Custom MapPoint")]    
    public AbstractMap _map;
    
    public Button _submitFeature;
    public GameObject _InputMapPointObject;
    public InputField _nameInput;

    public delegate void OnClickButton();
    public OnClickButton onClickAddButton;

    public OnClickButton onClickCancel;

    public MapSelector.SelectFeature onAddFeatureCallback;

    public enum SelectionMode
    {
        None,
        Location,
        Feature
    };

    public struct SelectionEntry
    {
        public GameObject gameObject;
        public Feature feature;

        public SelectionEntry(GameObject obj, Feature feature)
        {
            this.feature = feature;
            this.gameObject = obj;
        }
    }
    private List<SelectionEntry> _selectionList;
    private SelectionEntry _customFeature;

    private SelectionMode _selectionMode;

    private string _accessToken = "access_token=pk.eyJ1IjoiYW15a2hvb3ZlciIsImEiOiJjampweTJjbGs4MmRjM2twMTdzbWl6dTkyIn0.nHcaY9wzvR1jyEgAk81fjg";
    private string _writeToken = "access_token=sk.eyJ1IjoiYW15a2hvb3ZlciIsImEiOiJjamtkNmExbnkwNG56M3FwOGxlc2E0dG1sIn0.tZUP6QL4TP2ZVHr02RwhIw";

    private void Awake()
    {
        _addButton.onClick.AddListener(OnClickAddFeatureButton);
    }
    // Use this for initialization
    void Start () {
        _selectionList = new List<SelectionEntry>(5);
        _hotSpot = new Vector2(_cursorTex.width * .5f, _cursorTex.height);
	}

    public void ResetState()
    {
        if (_customFeature.gameObject != null)
        {
            Destroy(_customFeature.gameObject);
            _customFeature.gameObject = null;
            _customFeature = new SelectionEntry();
        }
        _InputMapPointObject.SetActive(false);
        _selectionMode = SelectionMode.None;
        _addButtonGO.SetActive(true);        
    }

    void SelectLocation()
    {
        Vector3 clickPos = Input.mousePosition;

        Ray ray = Camera.main.ScreenPointToRay(clickPos);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 1000.0f))
        {
            SetSelectionMode(SelectionMode.Feature);
            StartCoroutine(QueryPoints(hit.point));
        }
        else
        {
            SetSelectionMode(SelectionMode.None);
        }
    }

    void SelectFeature()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        bool hitEntry = false;
        if(Physics.Raycast(ray, out hit))
        {
            if (hit.collider.gameObject == _customFeature.gameObject)
            {
                EnableInfoMode();
                hitEntry = true;
            }
            else
            {
                foreach (var entry in _selectionList)
                {
                    if (entry.gameObject == hit.collider.gameObject)
                    {
                        //send query to update dataset
                        StartCoroutine(InsertFeatureToDataSet(entry));
                        hitEntry = true;
                    }
                }
            }
        }

        if(hitEntry)
            SetSelectionMode(SelectionMode.None);
    }

    void Update()
    {
        if(Input.GetMouseButtonDown(0))//left click
        {
            switch(_selectionMode)
            {
                case SelectionMode.Location:
                    SelectLocation();
                    break;
                case SelectionMode.Feature:
                    SelectFeature();
                    break;
                default:
                    break;
            }
        }

        if(Input.GetKeyDown(KeyCode.Escape))
            SetSelectionMode(SelectionMode.None);
    }

    void SetSelectionMode(SelectionMode newMode)
    {
        switch (_selectionMode)
        {
            case SelectionMode.Location:                
                break;
            case SelectionMode.Feature:
                Cursor.SetCursor(null, Vector2.zero, _cursorMode);
                break;
            default:
                break;
        }

        switch (newMode)
        {
            case SelectionMode.Feature:
                Cursor.SetCursor(_cursorTex, _hotSpot, _cursorMode);
                break;
            default:
                break;
        }


        _selectionMode = newMode;
    }    

    void SpawnCustomFeature(Feature feature, Vector3 point)
    {
        if (_customFeature.gameObject != null)
            Destroy(_customFeature.gameObject);

        Vector3 spawnPoint = point;
        spawnPoint.y += _customFeaturePrefab.GetComponent<BoxCollider>().size.y * _customFeaturePrefab.transform.localScale.y * .5f;

        //spawn a custom feature to add a new one
        var selectionGO = Instantiate(_customFeaturePrefab, spawnPoint, Quaternion.Euler(90.0f, 0, 0));
        _customFeature = new SelectionEntry(selectionGO, feature);

        EnableInfoMode();
    }

    public void OnClickAddFeatureButton()
    {
        SetSelectionMode(SelectionMode.Location);
    }

    void EnableInfoMode()
    {
        _InputMapPointObject.SetActive(true);
        _addButton.gameObject.SetActive(false);
        _cameraMovement.SetCanMove(false);
    }

    public void OnInputMapPointName(string value)
    {
        if (_customFeature.feature == null)
            return;

        if (_customFeature.feature.Properties.ContainsKey("name"))
            _customFeature.feature.Properties["name"] = value;
        else
            _customFeature.feature.Properties.Add("name", value);
    }

    public void OnSubmitCustomFeature()
    {
        if (_customFeature.feature == null)
        {
            Debug.LogError("ERROR: Custom Feature cannot be null");
            return;
        }

        if(_customFeature.feature.Properties["name"].ToString() != "")
        {
            StartCoroutine(InsertFeatureToDataSet(_customFeature));
            _nameInput.text = "";
            _InputMapPointObject.SetActive(false);
        }
    }

    public void OnClickCancel()
    {
        _cameraMovement.SetCanMove(true);
        _nameInput.text = "";
        ResetState();
        onClickCancel();        
    }

    void SpawnNewFeatures(List<Feature> features)
    {
        //spawn prefabs for up to 5 features near the clicked location
        foreach (Feature feature in features)
        {
            Vector3 spawnPoint = _map.GeoToWorldPosition(feature.Center);
            spawnPoint.y += _newFeaturePrefab.GetComponent<BoxCollider>().size.y * _newFeaturePrefab.transform.localScale.y * .5f;

            var selectGO = Instantiate(_newFeaturePrefab, spawnPoint, Quaternion.Euler(90.0f, 0, 0));
            string name = feature.PlaceName.Split(',')[0];
            var label = selectGO.GetComponent<CustomLabelTextSetter>();
            label.SetName(name);

            var entry = new SelectionEntry(selectGO, feature);
            _selectionList.Add(entry);
        }
    }

    IEnumerator QueryPoints(Vector3 point)
    {
        Vector2d vecLatLong = _map.WorldToGeoPosition(point);
        string latLong = vecLatLong.y + "," + vecLatLong.x + ".json?";
        Debug.Log("lat long " + vecLatLong);

        //string parameters = "types=poi&limit=5&";
        string parameters = "types=poi&limit=5";
        string postURL = "https://api.mapbox.com/geocoding/v5/mapbox.places/" + latLong  + parameters + "&" + _accessToken;

        WWW www = new WWW(postURL);
        yield return www;        

        if (www.error != null)
        {
            Debug.Log("QueryAdd www error: " + www.error);
        }
        else
        {
            var response = MapboxAccess.Instance.Geocoder.Deserialize<ForwardGeocodeResponse>(www.text);
            //SpawnNewFeatures(response.Features);

            Feature feature = new Feature();
            feature.Geometry = new Geometry();
            feature.Geometry.Coordinates = vecLatLong;
            feature.Properties = new Dictionary<string, object>();
            feature.Id = "custom " + DateTime.Now.ToString("hhmmss") +  UnityEngine.Random.Range(1, 1403213231).ToString();
            feature.Properties.Add("name", "");
            //im just gonna assume this is the same district as above
            if (response.Features.Count > 0)
                feature.Properties.Add("district", response.Features[0].Context[0]["text"].ToString());
            SpawnCustomFeature(feature, point);
        }
    }

    IEnumerator InsertFeatureToDataSet(SelectionEntry entry)
    {
        onAddFeatureCallback(entry.feature.Properties["name"].ToString(), entry.feature.Geometry.Coordinates.x.ToString(), entry.feature.Geometry.Coordinates.y.ToString(), "");

        //string tilesetId = "cjjxgjqlw015wksmet7qq0ip1-8ngzu";
        string myId = "amykhoover";
        string datasetId = "cjl5mxavj0vfm2qmxdngdsie0";
        string feature = Mapbox.Json.JsonConvert.SerializeObject(entry.feature);

        //"https://api.mapbox.com/datasets/v1/[myID]/[myDatasetID]/features/[myFeatureID]?access_token=[myAccessToken]";

        string postURL = string.Format(
            "https://api.mapbox.com/datasets/v1/{0}/{1}/features/{2}?{3}"
            , myId
            , datasetId
            , entry.feature.Id
            , _writeToken
        );

        //string postURL = "https://api.mapbox.com/datasets/v1/";


        //byte[] bytes = System.Text.Encoding.UTF8.GetBytes(feature);
        // initialize as PUT
        UnityWebRequest www = UnityWebRequest.Put(postURL, feature);
        www.SetRequestHeader("Content-Type", "application/json");
        // HACK!!! override method and convert to POST
        www.method = "PUT";

        /* PHASE:dsadsa
         * fafdfad
         * fdsafd
         * dfdfafs
         */
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log("ApplyDatasetToTileset www error: " + www.error);
        }
        else
        {
            Debug.Log("ApplyDatasetToTileset www = " + www.downloadHandler.text);
            StartCoroutine(ApplyDatasetToTileset());            
        }

        yield return null;
    }

    IEnumerator ApplyDatasetToTileset()
    {
        string myId = "amykhoover";
        string datasetId = "cjjxgjqlw015wksmet7qq0ip1";
        string tilesetId = "cjjxgjqlw015wksmet7qq0ip1-8ngzu";

        string postURL = string.Format(
            "https://api.mapbox.com/uploads/v1/{0}?{1}"
            , myId
            , _writeToken
        );

        string datasetTilesetInfo = string.Format(
            @"{{""name"":""MyTileSet"",""tileset"":""{0}.{1}"",""url"":""mapbox://datasets/{0}/{2}""}}"
            , myId
            , tilesetId
            , datasetId
        );
        byte[] bytes = System.Text.Encoding.UTF8.GetBytes(datasetTilesetInfo);
        // initialize as PUT
        UnityWebRequest www = UnityWebRequest.Put(postURL, bytes);
        www.SetRequestHeader("content-type", "application/json");
        // HACK!!! override method and convert to POST
        www.method = "POST";

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log("ApplyDatasetToTileset www error: " + www.error);
        }
        else
        {
            Debug.Log("ApplyDatasetToTileset www = " + www.downloadHandler.text);
            MapboxAccess.Instance.ClearAllCacheFiles();
        }
    }
}
