﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Mapbox.Geocoding;
using Mapbox.Utils;
using Mapbox.Unity.Map;
using Mapbox.Examples;
using Mapbox.Unity.MeshGeneration.Components;

public class MapSelector : MonoBehaviour
{
    public AbstractMap map;

    [Header("Mouse Features")]
    public Texture2D cursorTex;
    public CursorMode cursorMode = CursorMode.Auto;

    //select map UI objects
    [Header("Map Selection Objects")]
    public Image _panel;
    public GameObject _selectFeature;
    public GameObject _selectPoint;
    public GameObject _zoomContainer;
    public AddFeature _addFeature;

    public CameraMovement _cameraMovement;

    private Vector2 hotSpot;

    private bool _isMapPointQuestion;
    private bool _isAddingFeature;
    private bool _isSelectingMapFeature;

    public GameObject _mapPointPrefab;
    private GameObject _mapPointGO;
    private bool _isChoosingPoint;

    [SerializeField]
    private Image _selectPointImage;
    [SerializeField]
    private Text _selectPointText;

    public delegate void SelectFeature(string featureName, string featureLat, string featureLong, string featureType = "");
    public SelectFeature selectCallback;

    public delegate void OnClickButton();
    public OnClickButton onDisableQuestionUICallback;
    public OnClickButton onEnableQuestionUICallback;

    public struct MapPoint
    {
        public Vector2d latlong;
        public string name;
        public string type;
    };
    private MapPoint _mapPointToSubmit;

    private string accessToken = "access_token=pk.eyJ1IjoiYW15a2hvb3ZlciIsImEiOiJjampweTJjbGs4MmRjM2twMTdzbWl6dTkyIn0.nHcaY9wzvR1jyEgAk81fjg";

    private void Awake()
    {
    }

    private void Start()
    {
        _addFeature.onClickAddButton = OnClickAddFeature;
        _addFeature.onAddFeatureCallback = OnAddFeature;
        _addFeature.onClickCancel = OnClickCancelAddFeature;

        _isChoosingPoint = false;
    }

    private void OnEnable()
    {
    }

    public void SetActive(bool isMapPointQuestion)
    {
        _isMapPointQuestion = isMapPointQuestion;
        _isSelectingMapFeature = !isMapPointQuestion;
        _selectPoint.SetActive(isMapPointQuestion);
        _selectFeature.SetActive(!isMapPointQuestion);

        gameObject.SetActive(true);
        _cameraMovement.SetCanMove(true);
        _panel.enabled = false;
    }

    public void ResetState()
    {
        _isSelectingMapFeature = false;
        _isAddingFeature = false;
        _isMapPointQuestion = false;

        _selectFeature.SetActive(false);
        _selectPoint.SetActive(false);
        _addFeature.ResetState();
        _zoomContainer.SetActive(true);
        //_searchFeature.SetActive(false);
        //_searchInputField.text = "";

        _mapPointToSubmit = new MapPoint();
        _cameraMovement.ResetZoom();
        _cameraMovement.SetCanMove(false);

        if (_mapPointGO != null)
            Destroy(_mapPointGO);

        Cursor.SetCursor(null, Vector2.zero, cursorMode);
    }

    private void OnClickFeature(RaycastHit hit)
    {
        CustomFeatureBehaviour feature = hit.collider.gameObject.GetComponentInParent<CustomFeatureBehaviour>();
        if (feature != null)
        {
            _mapPointToSubmit.type = "";
            _mapPointToSubmit.name = feature.Data.Properties["name"].ToString();
            _mapPointToSubmit.latlong = map.WorldToGeoPosition(hit.point);

            if (_mapPointGO != null)
                Destroy(_mapPointGO);

            //spawn game object
            Vector3 spawnPoint = hit.point;
            spawnPoint.y += _mapPointPrefab.GetComponent<BoxCollider>().size.y * _mapPointPrefab.transform.localScale.y * .5f;
            _mapPointGO = Instantiate(_mapPointPrefab, spawnPoint, Quaternion.Euler(90.0f, 0.0f, 0.0f));
            _mapPointGO.GetComponent<CustomLabelTextSetter>()._textMesh.text = _mapPointToSubmit.name;
        }
    }

    private void SelectMapPoint(RaycastHit hit)
    {
        if (_mapPointGO != null)
            Destroy(_mapPointGO);

        _mapPointToSubmit.name = "";
        _mapPointToSubmit.type = "";
        _mapPointToSubmit.latlong = map.WorldToGeoPosition(hit.point);

        Vector3 spawnPoint = hit.point;
        spawnPoint.y += _mapPointPrefab.GetComponent<BoxCollider>().size.y * _mapPointPrefab.transform.localScale.y * .5f;
        _mapPointGO = Instantiate(_mapPointPrefab, spawnPoint, Quaternion.Euler(90.0f, 0.0f, 0.0f));

        _selectPointImage.enabled = true;
        _selectPointText.enabled = true;
        _isChoosingPoint = false;

        onEnableQuestionUICallback();
    }

    public void OnClickMapPointButton()
    {
        //get rid of UI that are in the way
        _isChoosingPoint = true;
        _selectPointImage.enabled = false;
        _selectPointText.enabled = false;

        //enable options for selecting features
        onDisableQuestionUICallback();
    }

    public void OnSubmit()
    {
        if(_isAddingFeature)
        {
            _addFeature.OnSubmitCustomFeature();
        }
        else if (_mapPointGO != null && (_isMapPointQuestion || (_isSelectingMapFeature && _mapPointToSubmit.name != "")))
        {
            Destroy(_mapPointGO);
            selectCallback(_mapPointToSubmit.name, _mapPointToSubmit.latlong.x.ToString(), _mapPointToSubmit.latlong.y.ToString());
        }
    }

    private void OnSubmitCustomFeature(string name, string lat, string lng, string type)
    {
        selectCallback(name, lat, lng, type);
        //ResetState();
    }

    public void OnClickAddFeature()
    {
        //this is handled by the AddFeature script attached to the button
        _isAddingFeature = true;
        if (_mapPointGO != null)
        {
            Destroy(_mapPointGO);
            _mapPointToSubmit = new MapPoint();
        }
    }

    public void OnClickCancelAddFeature()
    {
        _isAddingFeature = false;
    }

    public void OnAddFeature(string featureName, string lat, string lng, string type = "")
    {
        selectCallback(featureName, lat, lng, type);
        Debug.Log("Add feature name " + featureName);
        Debug.Log("Add feature latlong " + lat + " " + lng);
        Debug.Log("Add feature type " + type);

        //ResetState();
    }

    public void OnClickSelectFeatureButton()
    {
        //get rid of UI that are in the way
        _isSelectingMapFeature = true;

        //enable options for selecting features
        _zoomContainer.SetActive(true);
        _addFeature.gameObject.SetActive(true);

        onDisableQuestionUICallback();
    }

    // Update is called once per frame
    void Update()
    {
        if (_isAddingFeature)//let add feature script handle this
            return;

        if ((_isSelectingMapFeature || _isChoosingPoint) && Input.GetMouseButtonDown(0))
        {
            if (EventSystem.current.IsPointerOverGameObject())
                return;

            Vector3 clickPos = Input.mousePosition;
            Ray ray = Camera.main.ScreenPointToRay(clickPos);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 1000.0f))
            {
                if (_isChoosingPoint)
                    SelectMapPoint(hit);
                else if (_isSelectingMapFeature)
                    OnClickFeature(hit);
            }
        }
    }


}
