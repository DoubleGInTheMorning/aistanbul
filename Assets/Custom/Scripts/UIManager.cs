﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mapbox.Examples;
using Mapbox.Unity.Map;

public class UIManager : MonoBehaviour {

    public float maxIdleTime;
    private float idleTimeCounter;    
    public float displayTextTimer;


    public float _maxFinishTime;
    private float _finishTimer;    

    public Camera camera;
    public CameraMovement camMovement;

    public AbstractMap map;

    [System.Serializable]
    public enum ScreenState
    {
        ScreenSaver,
        Explanation,
        Tutorial,
        Question,
        Finish,
        //MapUI,
        None,
    };


    [System.Serializable]
    public struct CanvasEntry{
        public ScreenState name;
        public GameObject gameObj;
    }

    public CanvasEntry[] canvasArray;
    private Dictionary<ScreenState, GameObject> canvasDict;
    private GameObject activeCanvas;
    private ScreenState currState;

    public delegate void OnTransitionHandler(UIManager.ScreenState newState);
    static OnTransitionHandler transitionHandler;

    public delegate void OnSelectDistrictHandler(string district, Mapbox.Utils.Vector2d LongLat);
    static OnSelectDistrictHandler selectDistrictHandler;

    public static OnTransitionHandler GetTransitionHandler()
    {
        return transitionHandler;
    }

    public static OnSelectDistrictHandler GetSelectDistrictHandler()
    {
        return selectDistrictHandler;
    }    

    public Text newSessionTimerText;
    public Text newSessionTimer;

    public QuestionManager questionManager;
    public Button quitButton;

    private void Awake()
    {
        if (transitionHandler == null)
            transitionHandler = ChangeActiveState;

        //if (selectDistrictHandler == null)
        //    selectDistrictHandler = OnSelectDistrict;
    }

    void Start () {

		if(camera == null)
            Debug.LogError("Camera cannot be null");

        canvasDict = new Dictionary<ScreenState, GameObject>(canvasArray.Length);
        foreach (CanvasEntry entry in canvasArray)
        {
            canvasDict.Add(entry.name, entry.gameObj);
            entry.gameObj.gameObject.SetActive(false);            
        }

        if (canvasDict.ContainsKey(ScreenState.ScreenSaver))
        {
            activeCanvas = canvasDict[ScreenState.ScreenSaver];
            activeCanvas.gameObject.SetActive(true);
            currState = ScreenState.ScreenSaver;            
        }

        quitButton.onClick.AddListener(OnClickQuit);
	}
	
    void SetActiveCanvas(ScreenState canvasType)
    {
        activeCanvas.gameObject.SetActive(false);
        activeCanvas = canvasDict[canvasType];
        activeCanvas.gameObject.SetActive(true);
    }

    public void UpdateState()
    {
        switch (currState)
        {
            case ScreenState.Finish:
                _finishTimer -= Time.deltaTime;

                if (Input.anyKey || (Input.GetAxis("Mouse X") != 0) ||
                    (Input.GetAxis("Mouse Y") != 0) || Input.GetMouseButton(0) || Input.GetMouseButton(1))
                    _finishTimer = _maxFinishTime;

                if (_finishTimer <= 0.0f)
                {                   
                    ChangeActiveState(ScreenState.ScreenSaver);                    
                }
                break;
            default:
                if (maxIdleTime - idleTimeCounter <= displayTextTimer)
                    DisplayTimeLeft((int)(maxIdleTime - idleTimeCounter));
                break;
        }
    }
    public void ChangeActiveState(ScreenState newState)
    {
        if (currState == newState) return;

        switch (currState)
        {
            case ScreenState.Question:
                //track question answers until deletion
                questionManager.OnSessionEnd();
                break;
            default:
                break;
        }

        switch(newState)
        {
            case ScreenState.Question:
                //track question answers until deletion
                break;
            case ScreenState.Finish:
                _finishTimer = _maxFinishTime;
                break;
            default:
                break;
        }

        SetActiveCanvas(newState);
        currState = newState;
    }

    public void OnClickQuit()
    {
        ChangeActiveState(ScreenState.Finish);
    }

    public void DisplayTimeLeft(int time)
    {
        newSessionTimerText.gameObject.SetActive(true);
        newSessionTimer.text = time.ToString() + "s";
    }

	// Update is called once per frame
	void Update () {
        idleTimeCounter += Time.deltaTime;

        //TODO put in own function and add comments
        if (Input.anyKey || (Input.GetAxis("Mouse X") != 0) || (Input.GetAxis("Mouse Y") != 0) || Input.GetMouseButton(0) || Input.GetMouseButton(1))
        {
            idleTimeCounter = 0.0f;
            newSessionTimerText.gameObject.SetActive(false);
            if (currState == ScreenState.ScreenSaver)
            {
                ChangeActiveState(ScreenState.Explanation);
            }
        }        
        else if (idleTimeCounter >= maxIdleTime)
        {
            ChangeActiveState(ScreenState.ScreenSaver);
        }

        UpdateState();
	}
}
