﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mapbox.Unity.Map;

public class FadeScene : MonoBehaviour {
    
    public float _fadeTime;
    private float _timeRemaining;
    [SerializeField]
    private AbstractMap _map;
    [SerializeField]
    private Image _fadeImage;

    public delegate void FadeTransition();

    public enum State
    {
        Waiting,
        FadeOut,
        FadeIn,        
    }

    private State _currentState;
    private State _nextState;

    // Use this for initialization
    void Start () {
        _currentState = State.Waiting;
        _nextState = State.Waiting;
	}
	
	// Update is called once per frame
	void Update () {
		if(_nextState != State.Waiting && _currentState == State.Waiting)
        {
            if(_nextState == State.FadeIn)
                StartCoroutine(FadeIn(_fadeImage));
            else
                StartCoroutine(FadeOut(_fadeImage));

            _currentState = _nextState;
            _nextState = State.Waiting;
        }
	}

    public void StartFade(State state)
    {
        return;
        //TODO: figure out when map is loading new tiles & call this
        if(_currentState != state)
        {
            _nextState = state;
        }        
    }

    IEnumerator FadeOut(Image image)
    {
        float alpha;
        while (_timeRemaining > 0.0f)
        {
            _timeRemaining -= Time.deltaTime;
            alpha = Mathf.Lerp(.66f, 0.0f, _timeRemaining / (_fadeTime) - 1.0f);
            Color color = image.color;
            color.a = alpha;
            image.color = color;
            yield return new WaitForEndOfFrame();
        }
        _currentState = State.Waiting;
    }

    IEnumerator FadeIn(Image image)
    {
        float alpha;
        while (_timeRemaining > 0.0f)
        {
            _timeRemaining -= Time.deltaTime;
            alpha = Mathf.Lerp(0.0f, .66f, _timeRemaining / (_fadeTime));
            Color color = image.color;
            color.a = alpha;
            image.color = color;
            yield return new WaitForEndOfFrame();
        }
        _currentState = State.Waiting;
    }
}
